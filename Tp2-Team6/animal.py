
# TP2 - Equipe 6
# Alex Bouchard
# Anthony Sanou
# Gabriel Kirouac
# Hiba Ikbal
# Laurent Montreuil
# Ouail Slimani
# Wilfrido Quinteros Ramirez
# William Michaud

import random

from helper import Helper as H
from _ast import If
from controleur import *

class WorldObject():
    def __init__(self):
        self.exists = True
        
    def Exists(self):
        return self.exists
        
        
class Coord():
    def __init__(self, parent, x, y):
        self.parent = parent
        self.x = x 
        self.y = y      

class Animal(WorldObject):
    # Attributs static
    STATUS = { "STARVATION": 0 , "DEHYDRATION": 1 , "SLEEPY": 2, "HORNY": 3  }
    VISION_UPDATE_SPEED = 5
    GENDERS = {"MALE": 0, "FEMALE":1}      
        
    
    def __init__(self, parent=None, x=0, y=0, mom = None, dad = None):  # none c'est pour tester
        self.parent = parent
        self.id = self.parent.generateurid.prochainid()
        self.x = x
        self.y = y
        self.alive = True
        self.gender = random.choice([Animal.GENDERS["MALE"],Animal.GENDERS["FEMALE"] ]) # "Male ou Femelle",     50%

        self.mapName = None  # Pour intégration dans la vue        
        self.cible = None
        self.destination = None
        self.status = []
        self.meat = 100
        self.TimeRatio = self.parent.parent.TimeRatioControlleur
        
        # A.I. & ACTIONS
        self.currAction = None
        self.urgentNeed = None
        self.changeAction = 0
        
        self.fieldOfView = []
        self.foodTargets = []       # Représente la liste des choses que l'animal peut voir et qui peuvent influencer ses actions  
        self.ressourceTargets = []
        self.threatTargets = []
        self.reproductionTargets = []
        
        self.angle = 0               # Pour les déplacements vers une cible, il faut un angle
        self.updateVision = 0        # Compteur, quand atteint VISION_UPDATE_SPEED on update la vision   
            
            
        # Héritage génétique survient uniquement si l'animal est généré à partir d'une reproduction male/femelle
        if mom != None and dad != None:
            self.inherit_Parent_Attributes(mom,dad)
        else:            
            # VISION      
            self.visionDist = 1
            self.visionDistMax = 1
            self.dayVisionDist = self.visionDist
            self.nightvisionDist = self.visionDist
     
            # SPEED
            self.speed = 1 * self.TimeRatio
            self.speedMax = 1
            
            # SIZE
            self.sizeMin = 1 
            self.sizeMax = 1
            self.size = self.sizeMin
                    
            # NEEDS
            self.sicknessMin = 0
            self.sicknessMax = 100
            self.sicknessGain = 1
            self.sicknessLoss = 1
            self.sickness = self.sicknessMin
            
            self.satietyMax = 100
            self.satietyLoss = 1
            self.satiety = self.satietyMax
            
            self.hydrationMax = 100
            self.hydrationLoss = 1
            self.hydration = self.hydrationMax
            
            self.libidoMax = 100;
            self.libidoGain = 1
            self.libido = 0;       
            
            self.energyMax = 200
            self.sleepEffeciency = 5  # Quantité d'énergie restauré à chaque tick() lors d'un sommeil
            self.energyLoss = 1
            self.energy = self.energyMax
    
        
        # À intégrer dans le futur, peut-être    -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> -> ->
        self.age = 0
        self.ageStage = "babyAge/adultAge/oldAge"  # Stade de vieillesse   
        self.maxBabys = 1  # reproduction
        self.gestationPeriod = 1
        self.mom = "Chantal"
        self.kids = []        
        self.sleepSchedule = "Diurnal/Nocturnal"
        self.diet = "Meat/plant/rocks"  
        #  -> -> -> -> -> 
        
        
        # MÉTHODES
    # -----------------------------------------------------------------------------------------------------------------------------------------------
    def inherit_Parent_Attributes(self,mom,dad):    # Mutate également l'attribut. Faire deux fonctions séparé dans le futur si nécessaire
        # VISION
        self.visionDist = self.parent.mutate_Attribute(random.choice([mom.visionDist, dad.visionDist]), 1)  # Vision minimal = 1
        self.visionDistMax = self.parent.mutate_Attribute(random.choice([mom.visionDistMax, dad.visionDistMax]), 1)
        self.dayVisionDist = self.parent.mutate_Attribute(random.choice([mom.dayVisionDist, dad.dayVisionDist]), 1)
        self.nightvisionDist = self.parent.mutate_Attribute(random.choice([mom.nightvisionDist, dad.nightvisionDist]), 1)     
        
        # SPEED
        self.speed = self.parent.mutate_Attribute(random.choice([mom.speed, dad.speed]), 1)
        self.speedMax = self.parent.mutate_Attribute(random.choice([mom.speedMax, dad.speedMax]), 1)
        
        # SIZE
        self.sizeMin = self.parent.mutate_Attribute(random.choice([mom.sizeMin, dad.sizeMin]), 1)
        self.sizeMax = self.parent.mutate_Attribute(random.choice([mom.sizeMax, dad.sizeMax]), 1)
        self.size = self.sizeMin 
    
        # NEEDS
        self.sicknessMin = self.parent.mutate_Attribute(random.choice([mom.sicknessMin, dad.sicknessMin]), 0)
        self.sicknessMax = self.parent.mutate_Attribute(random.choice([mom.sicknessMax, dad.sicknessMax]), 1)
        self.sicknessGain = self.parent.mutate_Attribute(random.choice([mom.sicknessGain, dad.sicknessGain]), 1)
        self.sicknessLoss = self.parent.mutate_Attribute(random.choice([mom.sicknessLoss, dad.sicknessLoss]), 1)
        self.sickness = self.sicknessMin
        
        self.satietyMax = self.parent.mutate_Attribute(random.choice([mom.satietyMax, dad.satietyMax]), 1)
        self.satietyLoss = self.parent.mutate_Attribute(random.choice([mom.satietyLoss, dad.satietyLoss]), 1)
        self.satiety = self.satietyMax
        
        self.hydrationMax = self.parent.mutate_Attribute(random.choice([mom.hydrationMax, dad.hydrationMax]), 1)
        self.hydrationLoss = self.parent.mutate_Attribute(random.choice([mom.hydrationLoss, dad.hydrationLoss]), 1)
        self.hydration = self.hydrationMax
        
        self.libidoMax = self.parent.mutate_Attribute(random.choice([mom.libidoMax, dad.libidoMax]), 1)
        self.libidoGain = self.parent.mutate_Attribute(random.choice([mom.libidoGain, dad.libidoGain]), 1)
        self.libido = 0;     
        
        self.energyMax = self.parent.mutate_Attribute(random.choice([mom.energyMax, dad.energyMax]), 1)
        self.energyLoss = self.parent.mutate_Attribute(random.choice([mom.energyLoss, dad.energyLoss]), 1)
        self.sleepEffeciency  = self.parent.mutate_Attribute(random.choice([mom.sleepEffeciency , dad.sleepEffeciency ]), 1)
        self.energy = self.energyMax
    
    
    def move(self):         
        if self.cible == None:
            self.getcible() # Pour l'instant, l'animal change de cible une fois qu'elle est atteinte
        
        
        if self.is_Within_Reach(self.cible) == False:
            casex1 = int(self.x / self.parent.taillecase)  # case en x ou se trouve la position x
            casey1 = int(self.y / self.parent.taillecase)  # case en y ou se trouve la position y

            self.angle = H.calcAngle(self.x, self.y, self.cible.x, self.cible.y)       # Angle de déplacement
            self.x, self.y = H.getAngledPoint(self.angle, self.speed, self.x, self.y)  # change x,y pour se rapprocher de (valeur de speed) de la cible
           
            self.x = H.adapt_To_Max_Index(self.x,self.parent.largeurpx) # Empêche de dépasser vers le max
            self.x = H.adapt_To_Min_Index(self.x)                       # Empêche de dépasser vers le min
            self.y = H.adapt_To_Max_Index(self.y,self.parent.hauteurpx) # Empêche de dépasser vers le max
            self.y = H.adapt_To_Min_Index(self.y)                       # Empêche de dépasser vers le min  
                    
            casex2 = int(self.x / self.parent.taillecase)  # case en x ou se trouve la position x
            casey2 = int(self.y / self.parent.taillecase)  # case en y ou se trouve la position y

            self.parent.map[casey1][casex1][self.mapName].remove(self)  # Enlève l'animal de la coordonnée [casey1][casex1] du cadre tkinter
            self.parent.map[casey2][casex2][self.mapName].append(self)                
        else:
            self.getcible() # Pour l'instant, l'animal change de cible une fois qu'elle est atteinte
                    
    def getcible(self):
        self.fieldOfView = self.parent.getsubmap(self.x, self.y, self.visionDist)
        cibles = []
        for i in self.fieldOfView:
            if i["lapins"]:
                cibles = cibles + i["lapins"]
        if cibles:
            self.cible = random.choice(cibles)      
        else:
            x = self.x + random.randrange(100) - 50     # Position random à gauche ou a droite
            x = H.adapt_To_Max_Index(x,self.parent.largeurpx) # Empêche de dépasser vers le max
            x = H.adapt_To_Min_Index(x)                       # Empêche de dépasser vers le min
            
            y = self.y + random.randrange(100) - 50      # Position random en haut ou a gauche
            y = H.adapt_To_Max_Index(y,self.parent.hauteurpx) # Empêche de dépasser vers le max
            y = H.adapt_To_Min_Index(y)                       # Empêche de dépasser vers le min 
            
            self.cible = Coord(None,x,y)
    
    
    def is_Target(self):
        if self.cible == None:
            return False
        else:
            return self.cible.isInworld
                
            
    
    def find_Food_Target(self):
        # Dépend de la diet
        pass
    
    def find_Mating_Target(self):
        # dépend de la race
        pass
    
    def find_Water_Target(self):
        pass
    
    def is_A_Target_Nearby(self, targetName):
        for i in self.fieldOfView:
            if i[targetName]:
                return true 
            
    def pick_Nearest_Target(self, targetName):  # Trouve le target le plus proche. Retourne rien si il n'y en a pas
        nearestDist = None
        dist = None
        nearestTarget = None
        self.fieldOfView = self.parent.getsubmap(self.x, self.y, self.visionDist)   # Le field of view doit être à jour pour utiliser ça
        
        for i in self.fieldOfView:
            if i[targetName]:
                for j in i[targetName]:                    
                    if j != self:           # DISCLAIMER: doit trouver un moyen de s'enlever de son propre field of view!
                        if nearestDist == None:
                            nearestDist = H.calcDistance(self.x, self.y, j.x, j.y)
                            nearestTarget = j
                        else:
                            dist = H.calcDistance(self.x, self.y, j.x, j.y)
                            if  dist < nearestDist:
                                nearestDist = dist 
                                nearestTarget = j
                      
        return nearestTarget
                            
            
    
    
    """###################################### AI #################################################"""
    
    #ETAPE 1
    def update_Needs(self):
        self.decrease_Satiety()
        self.decrease_Hydration()
        self.increase_Libido()
        self.decrease_Energy()
        self.decrease_Sickness()
    
    
    def urgent_Need(self):
        
        minimum = self.satiety / self.satietyMax
        
        self.urgentNeed = "satiety"
        
        if self.hydration / self.hydrationMax < minimum:
            minimum = self.hydration / self.hydrationMax
            self.urgentNeed = "hydration"
            
        if self.energy / self.energyMax < minimum:
            minimum = self.energy / self.energyMax
            self.urgentNeed = "energy"
            
        #if self.libido == self.libidoMax:
        #    minimum = self.libido / self.libidoMax
        #    self.urgentNeed = "libido"
    
    
    #ETAPE 2    
    def update_Status(self):  # Applique l'effet des status, et retire ceux qui ne sont plus applicables
        for s in self.status:
            removeStatus = False
            
            if s == Animal.STATUS["STARVATION"]:  # STARVATION
                if self.satiety == 0:
                    self.decrease_Energy(2)  # Drain rapidement l'énergie et augmente le taux de sickness
                    self.increase_Sickness()
                else:
                    self.status.remove(Animal.STATUS["STARVATION"])
            
            elif s == Animal.STATUS["DEHYDRATION"]:  # DEHYDRATION
                if self.hydration == 0:
                    self.decrease_Energy(2)  # Drain rapidement l'énergie et augmente le taux de sickness
                    self.increase_Sickness()
                else:
                    self.status.remove(Animal.STATUS["DEHYDRATION"])
                    
                    
            elif s == Animal.STATUS["SLEEPY"]: #SLEEPY        
                    pass
                    
            elif s == Animal.STATUS["HORNY"]:  # HORNY
                if self.libido < self.libidoMax:
                    self.status.remove(Animal.STATUS["HORNY"])  # Ne fais rien pour l'instant
                    
    
    #ETAPE 3                
    def update_Vision(self):  # What do I perceive?
        self.update_Targets_Lists()     # Clear chaque objets qui sont trop loins du champs de vision   
        self.updateVision += 1
        if Animal.VISION_UPDATE_SPEED == self.updateVision:
            self.updateVision = 0
            self.update_Field_Of_View()       
    
    
    #ETAPE 4
    def make_Decision(self): #Enclanchement d'une action
        if self.status:
            if len(self.status) == 1:
                if self.status[0] == Animal.STATUS["STARVATION"]:
                    self.currAction = self.getFood
                elif self.status[0] == Animal.STATUS["DEHYDRATION"]:
                    self.currAction = self.getWater
                elif self.status[0] == Animal.STATUS["SLEEPY"]:
                    self.currAction = self.getSleep
                elif self.status[0] == Animal.STATUS["HORNY"]:
                    self.currAction = self.getLaid
                    
            else:
                status = random.choice(self.status)
                if status == Animal.STATUS["STARVATION"]:
                    self.currAction = self.getFood
                elif status == Animal.STATUS["DEHYDRATION"]:
                    self.currAction = self.getWater
                elif status == Animal.STATUS["SLEEPY"]:
                    self.currAction = self.getSleep
                elif status == Animal.STATUS["HORNY"]:
                    self.currAction = self.getLaid
                    
                    
        else:
            self.urgent_Need()
            if self.urgentNeed == "satiety":
                self.currAction = self.getFood
            if self.urgentNeed == "hydration":
                self.currAction = self.getWater
            if self.urgentNeed == "energy":
                self.currAction = self.getSleep
            if self.urgentNeed == "libido":
                self.currAction = self.getLaid
            
            
    #ETAPE 5
    def update_Action(self):
        if self.currAction != None:
            self.currAction()         
        self.move()
        #utiliser la variable action
        #Action va pointer vers une adresse (la methode qui s'actionnise (getfood for example))
        
        
        
    def getSleep(self):
        return
        self.getcible = None
        
        
        
    def getLaid(self):
        return
        self.getcible = None
        
        
        
    def getFood(self):
        return
        if self.cible:
            if H.calcDistance(self.x, self.y, self.cible.x, self.cible.y) > self.speed: #calcul la distance entre lobjet et le lapin wtv
                if self.cible.consumable.type == food:
                    self.eat(food)
                    self.action = None
                    self.cible = None
                else:
                    self.cible = self.pick_nearest_target()
                    if self.cible == None:
                        self.gen_Rand_Destination(50,50) #trouve une position aleatoire
            else:
                self.move(cible)
        else:
            self.cible = self.pick_nearest_target(food)
            if self.cible == None:
                self.gen_Rand_Destination(50,50) #trouve une position aleatoire
                
    def getWater(self):
        return
        if self.cible:
            if H.calcDistance(self.x, self.y, self.cible.x, self.cible.y) > self.speed: #calcul la distance entre lobjet et le lapin wtv
                if self.cible.consumable.type == water:
                    self.drink(water)
                    self.action = None
                    self.cible = None
                else:
                    self.cible = self.pick_nearest_target()
                    if self.cible == None:
                        self.gen_Rand_Destination(50,50) #trouve une position aleatoire
            else:
                self.move(cible)
        else:
            self.cible = self.pick_nearest_target(water)
            if self.cible == None:
                self.gen_Rand_Destination(50,50) #trouve une position aleatoire
        
    """###########################################################################################"""
    
    def add_Status(self, status):
        for s in self.status:
            if status == s:
                return False  # pas de duppliqué dans la liste de status
        
        self.status.append(status)
        return True
    
    def remove_Status(self, status):
        self.status.remove(status)
        
    def explore(self):
        pass

    def eat(self, food):    # En attente de la classe food/water
        amount = self.satietyMax - self.satiety
        food -= amount
        self.satiety += amount
        self.satiety = H.adapt_To_Max_Value(self.satiety,self.satietyMax)
            
    def drink(self, source):# En attente de la classe food/water
        amount = self.hydrationMax - self.hydration
        source-= amount
        self.hydration+= amount  
        self.hydration = H.adapt_To_Max_Value(self.hydration, self.hydrationMax)
    
    def sleep(self):
        self.energy += self.sleepEffeciency
        self.energy = H.adapt_To_Max_Value(self.energy, self.energyMax)
            
    def fuck(self, partner):
        self.libido = 0
        partner.libido = 0        
        # reproduce selon genre
    
    def reproduce(self):    # Définis dans les sous classe
        pass
    
    def reproduce_Randomly(self):    # Définis dans les sous classe
        pass
                   
    def die(self):
        self.alive = False  # :(
        self.parent.morts.append(self)     # # La donnée sur la mort est transféré au WORLD_DATA
        #print("Sickness lvl at death: ", self.sickness)
            
    def rot(self):  # Le sort réservé à nous tous. À part les mommies?
        self.meat -= 1        
        if self.meat == 0:
            self.reproduce_Randomly()         # Génère 1 nouvel animal pour chaque mort
            return False
        else:
            return True 

    # Sickness death 1.0
    def death_From_Sickness(self):  # Chance de mourir relatif au niveau de sickness
        return random.uniform(0, self.sicknessMax) <= self.sickness    
    # Note: même un taux très bas de sickness tue très rapidement à cause que cette méthode est appelé à chaque frame
    
    # Sickness death 2.0
    def death_From_Sickness2(self):  # Si sickness au-delà de 50% 
        if self.sickness >= self.sicknessMax / 2:
            return random.uniform(0, 99) < 5     # 5% chance de mourir à chaque Frame  
        else:
            return False
    
    # Sickness death 3.0
    def death_From_Sickness3(self): # Sickness tue automatiquement quand ça atteint 100%. Moins random et plus constant
        return self.sickness >= self.sicknessMax     
    
    # Sickness death 4.0
    def death_From_Sickness4(self): # Mix entre 2 et 3
        if self.sickness >= self.sicknessMax / 2: 
            if self.sickness < self.sicknessMax:
                return random.uniform(0, 99) < 5     # 5% chance de mourir à 50% de sickness   
            else:
                return True # Mort à 100% de sickness
    
    def check_Needs(self):
        pass       
    
    def decrease_Satiety(self):
        if self.satiety > 0:
            self.satiety -= self.satietyLoss
            if self.satiety <= 0:
                    self.add_Status(Animal.STATUS["STARVATION"])
                    self.satiety = 0  # 0 est le minimum
                           
    def decrease_Hydration(self):
        if self.hydration > 0:
            self.hydration -= self.hydrationLoss
            if self.hydration <= 0:
                    self.add_Status(Animal.STATUS["DEHYDRATION"])
                    self.hydration = 0  # 0 est le minimum
    
    def increase_Libido(self):
        if self.libido < self.libidoMax:
            self.libido += self.libidoGain  # À noter:  Satiété, hydration et énergie décroissent au fil du temps, mais libido augmente 
        elif self.libido >= self.libidoMax:
            self.add_Status(Animal.STATUS["HORNY"])
            self.libido = self.libidoMax     
    
    def decrease_Energy(self, multiplier=1):
            self.energy -= self.energyLoss  * multiplier
            
    def decrease_Sickness(self):
        if self.energy > 0 and self.sickness > self.sicknessMin:
            self.sickness -= self.sicknessLoss
            if self.sickness < self.sicknessMin:
                self.sickness = self.sicknessMin 
                
    def increase_Sickness(self):
        self.sickness += self.sicknessGain
            
    def update_Age(self):
        self.age += 1  # Nécessite delta time et la variable année/tick
            
    
    
    # TARGETS AND VISION    
    
    
    def is_Within_Reach(self, target):
        return H.calcDistance(self.x, self.y, target.x, target.y) < self.speed
    
    def is_Out_Of_Sight(self, target):
        return H.calcDistance(self.x, self.y, target.x, target.y) > self.visionDist
    
    def update_Targets_Lists(self):
        t = 0
        while t < len(self.foodTargets):
            if self.is_Out_Of_Sight(self.foodTargets[t]):
                del self.foodTargets[t]
                t-=1
            t+=1
        
        t = 0
        while t < len(self.ressourceTargets):
            if self.is_Out_Of_Sight(t):
                del self.ressourceTargets[t]
                t-=1
            t+=1
                
        t = 0
        while t < len(self.threatTargets):
            if self.is_Out_Of_Sight(t):
                del self.threatTargets[t]
                t-=1
            t+=1
        
        t = 0
        while t < len(self.reproductionTargets):
            if self.is_Out_Of_Sight(t):
                del self.reproductionTargets[t]
                t-=1
            t+=1     
    
    def update_Field_Of_View(self):
        #self.fieldOfView = self.parent.getsubmap(self.x, self.y, self.visionDist)
        pass
    
    
    def gen_Rand_Destination(self,maxX,maxY):   
        x = self.x + random.randrange(maxX * 2) - maxX
        x = H.adapt_To_Array_Size(x,self.parent.largeurpx) # Empêche de dépasser les dimensions du monde
        
        y = self.y + random.randrange(maxY * 2) - maxY
        y = H.adapt_To_Array_Size(y,self.parent.hauteurpx) # Empêche de dépasser les dimensions du monde  
                  
        #self.destination = Coord(self,x,y)  # Séparer la cible de la destination? 
        self.cible = Coord(self,x,y)  
        
            
            
    def update(self):  # Mise à jour de l'animal, ses besoins, son état, sa perception et ses actions
        if self.alive:    
            self.update_Age()
            self.update_Needs()
            self.update_Status()
            self.update_Vision()
            self.make_Decision()          
            self.update_Action()
                              
            # Section test
            #print("Animal: " , self.mapName)
            #self.eat(1)
            #self.drink(1)    
            #print("satiety: " , self.satiety)
            #print("hydration: " , self.hydration)
            #print("libido: " , self.libido)
            #print("energy: " , self.energy)          
            #self.pick_Nearest_Target("loups")           
            #print("-----------\n")
            # Section test
            if self.death_From_Sickness4():
                self.die()
                return True
        else:
            return self.rot() # Si retourne false, on élimine l'élément dans le modèle
            
            
class Loup(Animal):
    def __init__(self, parent, x, y, mom = None,dad= None):
        Animal.__init__(self, parent, x, y,mom,dad)
        
        self.mapName = "loups"
        self.delaipourbouger = random.randrange(20) + 20
        self.seuilreproduction = 10
        self.meat = 400
        
        
        self.visionDist = random.randrange(10) + 10
        
        
        # Stats par défaut des premiers loups créés
        if mom == None and dad == None:
             # VISION      
            self.visionDist = 15
            self.visionDistMax = 15
            self.dayVisionDist = self.visionDist
            self.nightvisionDist = self.visionDist
     
            # SPEED
            self.speed = 3 * self.TimeRatio
            self.speedMax = 10
            
            # SIZE
            self.sizeMin = 4 
            self.sizeMax = 4
            self.size = self.sizeMin
                    
            # NEEDS
            self.sicknessMin = 0
            self.sicknessMax = 100
            self.sicknessGain = 1
            self.sicknessLoss = 1
            self.sickness = self.sicknessMin
            
            self.satietyMax = 100
            self.satietyLoss = 1
            self.satiety = self.satietyMax
            
            self.hydrationMax = 100
            self.hydrationLoss = 1
            self.hydration = self.hydrationMax
            
            self.libidoMax = 100;
            self.libidoGain = 1
            self.libido = 0;       
            
            self.energyMax = 200
            self.sleepEffeciency = 5  # Quantité d'énergie restauré à chaque tick() lors d'un sommeil
            self.energyLoss = 1
            self.energy = self.energyMax
            return
        
        
       
    def reproduce(self):
        self.parent.genererloup()
        
        
    def reproduce_Randomly(self):    
        if len(self.parent.loups) > 1:
            loup = self
            while loup == self:
                loup = random.choice(self.parent.loups)
            self.parent.genererloup([self.x,self.y],self,loup)
        else:
            self.reproduce()
                    
        
class Lapin(Animal):
    def __init__(self, parent, x, y, mom = None, dad = None):
        Animal.__init__(self, parent, x, y,mom ,dad)
        self.mapName = "lapins"
        
        self.seuilreproduction = 30
        self.meat = 100
    
        # Stats par défaut des premiers lapins créés
        if mom == None and dad == None:
             # VISION      
            self.visionDist = 10
            self.visionDistMax = 10
            self.dayVisionDist = self.visionDist
            self.nightvisionDist = self.visionDist
     
            # SPEED
            self.speed = 5 * self.TimeRatio
            self.speedMax = 8
            
            # SIZE
            self.sizeMin = 4 
            self.sizeMax = 4
            self.size = self.sizeMin
                    
            # NEEDS
            self.sicknessMin = 0
            self.sicknessMax = 100
            self.sicknessGain = 1
            self.sicknessLoss = 1
            self.sickness = self.sicknessMin
            
            self.satietyMax = 100
            self.satietyLoss = 1
            self.satiety = self.satietyMax
            
            self.hydrationMax = 100
            self.hydrationLoss = 1
            self.hydration = self.hydrationMax
            
            self.libidoMax = 50;    # They reproduce a lot
            self.libidoGain = 1
            self.libido = 0;       
            
            self.energyMax = 200
            self.sleepEffeciency = 5  # Quantité d'énergie restauré à chaque tick() lors d'un sommeil
            self.energyLoss = 1
            self.energy = self.energyMax
        
    def getcible(self):
        self.gen_Rand_Destination(50,50)
    
    def reproduce(self):
        self.parent.genererlapin()
        # self.parent.genererlapin([self.x,self.y])
        
    def reproduce_Randomly(self):    # Prend 1 autre lapin random sur la carte, et créer 1 nouveau lapin avec les attributs partagés
        if len(self.parent.lapins) > 1:
            bunny = self
            while bunny == self:  
                bunny = random.choice(self.parent.lapins)
            self.parent.genererlapin([self.x,self.y],self,bunny)
        else:
            self.reproduce()
    
    
    
"""
        
#if __name__ == '__main__':   

    
    
    # Ceci print le nom de tout les attributs de la class
    # attributes = vars(animal)
    # for i in attributes:
    #    print(i)
"""
