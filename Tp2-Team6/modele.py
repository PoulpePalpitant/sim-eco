# TP2 - Equipe 6
# Alex Bouchard
# Anthony Sanou
# Gabriel Kirouac
# Hiba Ikbal
# Laurent Montreuil
# Ouail Slimani
# Wilfrido Quinteros Ramirez
# William Michaud


import random
import time
import math

from helper import Helper as H
from animal import *


class Generateurid():
    id=0
    def prochainid():
        Generateurid.id+=1
        return Generateurid.id

class Framboisier():
    def __init__(self,parent,x,y):
        self.parent=parent
        self.x=x
        self.y=y
        self.size=4
        self.NutitrionalValue=10
        self.age=random.randrange(10,20);
        self.alive=1
        
    def joueraction(self):
        self.gerercroissance()
        if self.age==200:
            self.die()
            self.parent.vegeMod=1
            
    def gerercroissance(self):
        self.age+=1
        if self.age%90==0:
            self.size+=1
            self.parent.vegeMod=1
    
    def die(self):
        self.alive=0
            
class Legume():
    def __init__(self,parent,x,y):
        self.parent=parent
        self.x=x
        self.y=y
        self.size=4
        self.NutitrionalValue=15
        self.age=random.randrange(10,20);
        self.alive=1
    
    def joueraction(self):
        self.gerercroissance()
        if self.age==160:
            self.die()
            self.parent.vegeMod=1
        
    def gerercroissance(self):
        self.age+=1
        if self.age%80==0:
            self.size+=1
            self.parent.vegeMod=1
            
    def die(self):
        self.alive=0

class Arbre():
    def __init__(self,parent,x,y):
        self.parent=parent
        self.x=x
        self.y=y
        self.size=random.randrange(5,15)
        self.age=random.randrange(50,100);
        self.NutitrionalValue=5
        self.alive=1
    
    def joueraction(self):
        self.gerercroissance()
        if self.age==2000:
            self.die()
            self.parent.vegeMod=1
    
    def gerercroissance(self):
        self.age+=1
        if self.age%300==0:
            self.size+=1
            self.parent.vegeMod=1
            
    def die(self):
        self.alive=0
         
       
class Carte():
    def __init__(self,parent,largeur=800,hauteur=600,taillecase=10,
                 nbforets=3,nbplansdeau=4,nbdeserts=6,nbplaines=10,
                 nbmontagnes=5,nbarbres=200,nblapins=100,nbloups=100,
                 nbframboisiers=30,nbLegumes=20):
        self.parent=parent
        self.generateurid=Generateurid
        self.largeurpx=largeur
        self.hauteurpx=hauteur
        self.taillecase=taillecase
        self.demicase=self.taillecase/2
        self.largeurcase=int(self.largeurpx/self.taillecase)
        self.hauteurcase=int(self.hauteurpx/self.taillecase)
        self.zoom=1
        self.vegeMod=0
        self.arbres=[]
        self.framboisiers=[]
        self.legumes=[]
        self.loups=[]
        self.lapins=[]
        self.morts=[]
        self.metaData=Metadata(self)
        self.map=self.makemat()
        self.regions={"forets":[nbforets,3,8],
                      "plansdeau":[nbplansdeau,2,6],
                      "deserts":[nbdeserts,4,10],
                      "plaines":[nbplaines,17,20],
                      "montagnes":[nbmontagnes,5,8]}
        self.creerregions()
        self.peuplersimulation(nbarbres,nblapins,nbloups,nbframboisiers,nbLegumes)
        
        #Genetics
        self.geneticVariance = 0.01     # % de variance génétique sur chaque attributs d'un objet du monde
        self.mutationProbability = 0.01 # % de chance qu'une variance génétique est lieu
        
    def creerregions(self):
        for i,j in self.regions.items():
            nbregions=j[0]
            listeregions=[]
            for k in range(nbregions):
                tailleregion=random.randrange(j[1])+j[2]
               
                startx=random.randrange(self.largeurcase)
                starty=random.randrange(self.hauteurcase)
                mapregion=self.getsubmap(startx*self.taillecase,starty*self.taillecase,tailleregion)
                print(i,mapregion)
                for m in mapregion:
                    m["terrain"]=i
                listeregions.append(mapregion)
            self.regions[i]=listeregions
                   
    def update(self):
        for i in self.lapins:
            if i.update() == False:     # POUR TESTER LA MÉTHODE UPDATE
                self.lapins.remove(i)   # Supprime l'animal du monde
                self.map[int(i.y /self.taillecase)][int(i.x/self.taillecase)]["lapins"].remove(i)   # Retire l'élément de la map

        for i in self.loups:
            if i.update() == False:
                self.loups.remove(i)   
                self.map[int(i.y /self.taillecase)][int(i.x/self.taillecase)]["loups"].remove(i)   
            
        for i in self.arbres:
            i.joueraction()
        for i in self.framboisiers:
            i.joueraction()
        for i in self.legumes:
            i.joueraction()
        
        self.kill_Vegetation()
   
    def peuplersimulation(self,nbarbres,nblapins,nbloups,nbframboisiers,nbLegumes):
        for i in range(nbarbres):
            p=self.genererarbre()
        for i in range(nbframboisiers):
            p=self.generer_Framboisier()
        for i in range(nbLegumes):
            p=self.generer_Legume()
        for i in range(nblapins):
            p=self.genererlapin()
        for i in range(nbloups):
            p=self.genererloup()
            
    def kill_Vegetation(self):
        for i in self.arbres:
            if(i.alive==0):
                self.arbres.remove(i)
                self.map[int(i.y /self.taillecase)][int(i.x/self.taillecase)]["arbres"].remove(i)
        for i in self.framboisiers:
            if(i.alive==0):
                self.framboisiers.remove(i)
                self.map[int(i.y /self.taillecase)][int(i.x/self.taillecase)]["framboisiers"].remove(i)
        for i in self.legumes:
            if(i.alive==0):
                self.legumes.remove(i)
                self.map[int(i.y /self.taillecase)][int(i.x/self.taillecase)]["legumes"].remove(i)
                
                
        
    def genererarbre(self):
        listeforets=self.regions["forets"]
        listeplaines=self.regions["plaines"]
        if((len(self.arbres)%4)==0):
            f=random.choice(listeplaines)
        else:
            f=random.choice(listeforets)
        case=random.choice(f)
        x=random.randrange(self.taillecase)+(case["casex"]*self.taillecase)
        y=random.randrange(self.taillecase)+(case["casey"]*self.taillecase)
        
        p=Arbre(self,x,y)
        case["arbres"].append(p) 
        self.arbres.append(p)
    
    def generer_Framboisier(self):
        listeforets=self.regions["forets"]
        listeplaines=self.regions["plaines"]
        if((len(self.framboisiers)%4)==0):
            f=random.choice(listeplaines)
        else:
            f=random.choice(listeforets)
        case=random.choice(f)
        x=random.randrange(self.taillecase)+(case["casex"]*self.taillecase)
        y=random.randrange(self.taillecase)+(case["casey"]*self.taillecase)
        
        p=Framboisier(self,x,y)
        case["framboisiers"].append(p) 
        self.framboisiers.append(p)
        
    def generer_Legume(self):
        listeforets=self.regions["forets"]
        listeplaines=self.regions["plaines"]
        if((len(self.legumes)%4)==0):
            f=random.choice(listeplaines)
        else:
            f=random.choice(listeforets)
        case=random.choice(f)
        x=random.randrange(self.taillecase)+(case["casex"]*self.taillecase)
        y=random.randrange(self.taillecase)+(case["casey"]*self.taillecase)
        
        p=Legume(self,x,y)
        case["legumes"].append(p) 
        self.legumes.append(p)    
            
    def genererloup(self,pos=[],mom = None, dad = None):
        if pos:
            x,y=pos
        else:
            x=random.randrange(self.largeurpx)
            y=random.randrange(self.hauteurpx)
        p=Loup(self,x,y,mom,dad)
        self.metaData.add_Loup()
        self.map[int(y/self.taillecase)][int(x/self.taillecase)]["loups"].append(p) 
        self.loups.append(p)
        p.getcible()
        return p
            
    def genererlapin(self,pos=[],mom = None, dad = None):
        if pos:
            x,y=pos
        else:
            x=random.randrange(self.largeurpx)
            y=random.randrange(self.hauteurpx)
        p=Lapin(self,x,y,mom,dad)
        self.metaData.add_Lapin()
        self.map[int(y/self.taillecase)][int(x/self.taillecase)]["lapins"].append(p) 
        self.lapins.append(p)
        p.getcible()
        return p
             
    def makemat(self):
        t1=[]
        for i in range(self.hauteurcase):
            t2=[]
            for j in range(self.largeurcase):
                t2.append({"casex":j,
                           "casey":i,
                           "terrain":"pre",
                           "lapins":[],
                           "loups":[],
                           "arbres":[],
                           "morts":[],
                           "framboisiers":[],
                           "legumes":[]})
            t1.append(t2)
        return t1
    
    
    def getsubmap(self,x,y,d):
        # case d'origine en cx et cy,  pour position pixels x, y
        cx=int(x/self.taillecase) 
        cy=int(y/self.taillecase) 
        # possible d'etre dans une case trop loin
        if cx==self.largeurcase:
            cx-=1
        if cy==self.hauteurcase:
            cy-=1
        
        # le centre en pixels de la case d'origine
        pxcentrex=(cx*self.taillecase)+self.demicase
        pxcentrey=(cy*self.taillecase)+self.demicase
        
        # la case superieur gauche de la case d'origine
        casecoinx1=cx-d
        casecoiny1=cy-d
        
        # assure qu'on deborde pas
        casecoinx1 = H.adapt_To_Min_Index(casecoinx1)
        casecoiny1 = H.adapt_To_Min_Index(casecoiny1)
        
        # la case inferieur droite
        casecoinx2=cx+d
        casecoiny2=cy+d
        
        # assure qu'on deborde pas
        casecoinx2 = H.adapt_To_Max_Index(casecoinx2, self.largeurcase)
        casecoiny2 = H.adapt_To_Max_Index(casecoiny2, self.hauteurcase)

        distmax=(d*self.taillecase)+self.demicase
        
        t1=[]
        for i in range(casecoiny1,casecoiny2):
            for j in range(casecoinx1,casecoinx2):
                case=self.map[i][j]
                pxcentrecasex=(j*self.taillecase)+self.demicase
                pxcentrecasey=(i*self.taillecase)+self.demicase
                distcase=H.calcDistance(pxcentrex,pxcentrey,pxcentrecasex,pxcentrecasey)
                if distcase<=distmax:
                    t1.append(case)
        return t1  
    
    
    
    def mutate_Attribute(self, attribute, min = None, max = None):    # Augmentation ou diminution d'un attribut selon la variance génétique et sa probabilité
        if random.random() <= self.mutationProbability:
            sign = H.Rand_Sign()
            attribute += attribute * self.geneticVariance * sign 
            
            # Fixation de limites pour ne pas brisé la simulation: exemple: si Vision distance ou size = -1: toute pète! 
            if min != None and max != None:
                attribute = H.adapt_To_Interval()
            elif min != None and max is None:
                attribute = H.adapt_To_Min_Value(attribute,min)
            elif min is None and max != None:
                attribute = H.adapt_To_Max_Value(attribute,max)
                          
        return attribute

class Metadata():
    def __init__(self, parent):
        self.nbAnimalsAlive = 0     #Currently alive
        self.nbAnimalsDead = 0      #Total deaths
        self.nbAnimalsTot = 0       #Nb of animals created since beginning
        self.sicknessLevelAtDeath = 0
        
        self.nbLapinsAlive = 0
        self.nbLapinsDead = 0
        self.nbLapinsTot = 0
        self.lapinSicknessLevelAtDeath = 0
        
        self.nbLoupsAlive = 0
        self.nbLoupsDead = 0
        self.nbLoupsTot = 0
        self.loupSicknessLevelAtDeath = 0
        
        
    def add_Lapin(self):
        self.nbAnimalsAlive +=1
        self.nbAnimalsTot += 1
        self.nbLapinsAlive += 1
        self.nbLapinsTot += 1
        print(self.nbAnimalsAlive)        
        
    def add_Loup(self):
        self.nbAnimalsAlive +=1
        self.nbAnimalsTot += 1
        self.nbLoupsAlive += 1
        self.nbLoupsTot += 1 
        print(self.nbAnimalsAlive)
               
    def remove_Animal(self, animal):
        self.animal=animal
        self.nbAnimalsAlive -= 1
        print(self.nbAnimalsAlive)
        #self.nbLoupsDead += 1
        #self.loupSicknessLevelAtDeath += 1
        
  
if __name__ == '__main__':
    test=Carte("test")
    print(test.largeurcase)
    print(test.hauteurcase)
