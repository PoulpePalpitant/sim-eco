from tkinter import *
import tkinter as tk
from tkinter import ttk, Checkbutton, IntVar
from controleur import *
import vue 
import modele

LARGEFONT =("Verdana", 10)

class tkinterApp(tk.Tk):
     
        # __init__ function for class tkinterApp  
        def __init__(self, *args, **kwargs):
     
            # __init__ function for class Tk 
            tk.Tk.__init__(self, *args, **kwargs)
     
            # creating a container 
            container = tk.Frame(self)
            container.pack(side = "top", fill = "both", expand = True)
            container.grid_rowconfigure(0, weight = 1)
            container.grid_columnconfigure(0, weight = 1)
            
    
            # initializing frames to an empty array 
            self.frames = {}
     
            # iterating through a tuple consisting 
            # of the different page layouts 
            for F in (SummPage, Page1, Page2, Page3):
     
                frame = F(container, self)
     
                # initializing frame of that object from 
                # Menu, page1, page2 respectively with  
                # for loop 
                self.frames[F] = frame
     
                frame.grid(row = 0, column = 0, sticky ="nsew")
     
            self.show_frame(SummPage)
     
            # to display the current frame passed as
        # parameter/ not open another tk window
        def show_frame(self, cont):
            frame = self.frames[cont]
            frame.tkraise()
     
        # first window frame startpage
     
class SummPage(tk.Frame):
        def __init__(self, parent, controller):
            
            tk.Frame.__init__(self, parent)
     
            # label of frame Layout 2 
            label = ttk.Button(self,text ="1.Créer une nouvelle simulation par défaut")
            #call new simu 
           
            
            # putting the grid in its place by using 
            # grid 
            label.grid(row = 0, column = 2, padx =10, pady = 10)
     
            button1 = ttk.Button(self, text ="2.Créer une simulation personnalisée",
                                 command = lambda : controller.show_frame(Page1))
     
            # putting the button in its place by 
            # using grid 
            button1.grid(row = 1, column = 2, padx = 10, pady = 10)
     
            ## button to show frame 2 with text layout2 
            button2 = ttk.Button(self, text ="3.Charger une simulation")
     
            # putting the button in its place by 
            # using grid 
            button2.grid(row = 2, column = 2, padx = 10, pady = 10)
     
     
     
     
        # second window frame page1
class Page1(tk.Frame):
     
        def __init__(self, parent, controller):
     
            tk.Frame.__init__(self, parent)
            
            label = ttk.Label(self, text ="Menu de création - Environement", font = LARGEFONT)
            
            label.grid(row = 1, column =2, padx = 10, pady = 10) 

            var1 = IntVar()
            Checkbutton1 =ttk.Checkbutton(self, text="Forêt", variable=var1).grid(row=2, sticky=W,column = 1)
            var2 = IntVar()
            Checkbutton2 =ttk.Checkbutton(self, text="Lacs", variable=var2).grid(row=3, sticky=W,column = 1)
            var3 = IntVar()
            Checkbutton3 =ttk.Checkbutton(self, text="Champs", variable=var3).grid(row=4, sticky=W,column = 1)
            var4 = IntVar()
            Checkbutton4 =ttk.Checkbutton(self, text="Marécages", variable=var4).grid(row=5, sticky=W,column = 1)
            var5 = IntVar()
            Checkbutton5 =ttk.Checkbutton(self, text="Montagnes", variable=var5).grid(row=2, sticky=W, column=3)
            var6 = IntVar()
            Checkbutton6 =ttk.Checkbutton(self, text="Déserts", variable=var6).grid(row=3, sticky=W,column=3)
            var7 = IntVar()
            Checkbutton7 =ttk.Checkbutton(self, text="Clairières", variable=var7).grid(row=4, sticky=W,column=3)
            
            # button to show frame 2 with text 
            # layout2 
            button1 = ttk.Button(self, text ="Menu Principal",
                                 command = lambda : controller.show_frame(SummPage))
     
            # putting the button in its place  
            # by using grid 
            button1.grid(row = 6, column = 3, padx = 10, pady = 10)
     
            # button to show frame 2 with text 
            # layout2 
            button2 = ttk.Button(self, text ="Suivant",
                                 command = lambda : controller.show_frame(Page2))
     
            # putting the button in its place by  
            # using grid 
            button2.grid(row = 7, column = 3, padx = 10, pady = 10)

        
        
     
     
        # third window frame page2
class Page2(tk.Frame):
        def __init__(self, parent, controller):
            tk.Frame.__init__(self, parent)            
            label = ttk.Label(self, text ="Menu de création- Animaux", font = LARGEFONT)            
            label.grid(row = 0, column=3,padx=10, pady = 10)
            
            var1 = IntVar()
            Checkbutton1 =ttk.Checkbutton(self, text="Loups", variable=var1).grid(row=1, sticky=W,column =1)           
            var2 = IntVar()
            Checkbutton2 =ttk.Checkbutton(self, text="Lapins", variable=var2).grid(row=2, sticky=W,column =1)
            var3 = IntVar()
            Checkbutton3 =ttk.Checkbutton(self, text="Lions", variable=var3).grid(row=3, sticky=W,column =1)
            var4 = IntVar()
            Checkbutton4 =ttk.Checkbutton(self, text="Ours", variable=var4).grid(row=4, sticky=W,column =1)
            var5 = IntVar()
            Checkbutton5 =ttk.Checkbutton(self, text="Éléphants", variable=var5).grid(row=5, sticky=W,column =1)
            var6 = IntVar()
            Checkbutton6 =ttk.Checkbutton(self, text="Hiboux", variable=var6).grid(row=6, sticky=W,column =1)
            var7 = IntVar()
            Checkbutton7 =ttk.Checkbutton(self, text="Aigles", variable=var7).grid(row=7, sticky=W,column =1)
            var8 = IntVar()
            Checkbutton8 =ttk.Checkbutton(self, text="Pigeons", variable=var2).grid(row=8, sticky=W,column =1)
            
            
            var9 = IntVar()
            Checkbutton9 =ttk.Checkbutton(self, text="crocodiles", variable=var9).grid(row=1, sticky=W, column = 4)
            var10 = IntVar()
            Checkbutton10 =ttk.Checkbutton(self, text="Grenouilles", variable=var10).grid(row=2, sticky=W,column =4 )
            var11= IntVar()
            Checkbutton11 =ttk.Checkbutton(self, text="Chévreuils", variable=var11).grid(row=3, sticky=W, column =4)
            var12 = IntVar()
            Checkbutton12 =ttk.Checkbutton(self, text="Raton-Laveurs", variable=var12).grid(row=4, sticky=W,column =4)
            var13 = IntVar()
            Checkbutton13 =ttk.Checkbutton(self, text="Poisson", variable=var13).grid(row=5, sticky=W,column = 4)
            var14 = IntVar()
            Checkbutton14 =ttk.Checkbutton(self, text="Mouettes", variable=var14).grid(row=6, sticky=W,column = 4)
            var15 = IntVar()
            Checkbutton15 =ttk.Checkbutton(self, text="Corbeaux", variable=var15).grid(row=7, sticky=W,column = 4)
            # button to show frame 2 with text 
            # layout2 
            button1 = ttk.Button(self, text ="Retour",
                                command = lambda : controller.show_frame(Page1))
     
            # putting the button in its place by  
            # using grid 
            button1.grid(row = 9, column = 4, padx = 10, pady = 10)
     
            # button to show frame 3 with text 
            # layout3 
            button2 = ttk.Button(self, text ="Suivant",
                                 command = lambda : controller.show_frame(Page3))
     
            # putting the button in its place by 
            # using grid 
            button2.grid(row = 10, column = 4, padx = 10, pady = 10)

     
class Page3(tk.Frame):
        def __init__(self, parent, controller):
            tk.Frame.__init__(self, parent)
            label = ttk.Label(self, text ="Menu de création- Végétaux", font = LARGEFONT)
            label.grid(row = 1, column =3, padx = 10, pady = 10)
            var1 = IntVar()
            Checkbutton1 =ttk.Checkbutton(self, text="Arbres", variable=var1).grid(row=3, sticky=W,column = 2)
            var2 = IntVar()
            Checkbutton2 =ttk.Checkbutton(self, text="Herbe", variable=var2).grid(row=4, sticky=W,column = 2)
            var3 = IntVar()
            Checkbutton3 =ttk.Checkbutton(self, text="Arbres fruitiers", variable=var3).grid(row=3, sticky=W,column = 4)
            var4 = IntVar()
            Checkbutton4 =ttk.Checkbutton(self, text="Légumes", variable=var4).grid(row=4, sticky=W,column = 4)
            # button to show frame 2 with text 
            # layout2 
            button1 = ttk.Button(self, text ="Lancer la simulation",
                                command = lambda : controller.show_frame(Page2))
     
            # putting the button in its place by  
            # using grid 
            button1.grid(row = 6, column = 4, padx = 10, pady = 10)
     
            # button to show frame 3 with text 
            # layout3 
            button2 = ttk.Button(self, text ="Retour")
            #call stimu 
     
            # putting the button in its place by 
            # using grid 
            button2.grid(row = 7, column = 4, padx = 10, pady = 10)
            


        
     
     
app = tkinterApp()
app.mainloop()


