# TP2 - Equipe 6
# Alex Bouchard
# Anthony Sanou
# Gabriel Kirouac
# Hiba Ikbal
# Laurent Montreuil
# Ouail Slimani
# Wilfrido Quinteros Ramirez
# William Michaud

import random
import time
import math
import modele
import vue

class Controleur():
    def __init__(self):
        self.modele=None 
        self.vue= vue.Vue(self)
        self.nbBoucle=0
        self.x=0
        self.y=0
        self.casevision=5
        self.ciblevision=-1
        self.idprochaineboucle=None
        self.subcourant=None
        self.TimeRatioControlleur = 1
        self.vue.root.mainloop()
    
    def demarrersimulation(self,taillex,tailley,taillecase,initrnd,nbforets,nbplansdeau,nbdeserts,nbplaines,nbmontagnes,nbarbres,nblapins,nbloups,nbframboisiers,nbLegumes):
        self.arretersimulation()
        random.seed(int(initrnd))
        self.nbBoucle=0
        self.modele=modele.Carte(self,int(taillex),int(tailley),int(taillecase),
                          int(nbforets),int(nbplansdeau),int(nbdeserts),int(nbplaines),int(nbmontagnes),
                          int(nbarbres),int(nblapins),int(nbloups),int(nbframboisiers),int(nbLegumes))
        self.vue.modele=self.modele
        self.subcourant=self.modele.getsubmap(self.x,self.y,self.casevision)
        self.depart=time.time()
        self.vue.dessineterrain(self.modele.map)
        self.vue.root.after(40,self.bouclesimulation)
        
    def arretersimulation(self):
        if self.idprochaineboucle:
            self.vue.root.after_cancel(self.idprochaineboucle)
        
    def bouclesimulation(self):
        self.nbBoucle+=1
        self.modele.update()
        if self.nbBoucle% self.vue.vitesseSim.get()==0:
            if self.ciblevision==1:
                self.vue.dessinecarte([self.subcourant])
            else:
                self.vue.dessinecarte(self.modele.map)
        self.idprochaineboucle=self.vue.root.after(1,self.bouclesimulation)
    
   

    def setsubcourant(self,x,y):
        self.x=x
        self.y=y
        self.subcourant=self.modele.getsubmap(self.x,self.y,self.casevision)
   
if __name__ == '__main__':
    c=Controleur()
    print("FINI")
    
    
    
    
    
    
    
    