# TP2 - Equipe 6
# Alex Bouchard
# Anthony Sanou
# Gabriel Kirouac
# Hiba Ikbal
# Laurent Montreuil
# Ouail Slimani
# Wilfrido Quinteros Ramirez
# William Michaud

import math
import random

class Helper(object):
    def getAngledPoint(angle,longueur,cx,cy):
        x = (math.cos(angle)*longueur)+cx
        y = (math.sin(angle)*longueur)+cy
        return (x,y)
    getAngledPoint = staticmethod(getAngledPoint)
    
    def calcAngle(x1,y1,x2,y2):
        dx = x2-x1
        dy = y2-y1
        #angle = (math.atan2(dy,dx) % (2*math.pi)) * (180/math.pi)
        angle = (math.atan2(dy,dx) ) #% (2*math.pi)) * (180/math.pi)
        return angle
    calcAngle = staticmethod(calcAngle)
    
    def calcDistance(x1,y1,x2,y2):
        dx = abs(x2-x1)**2
        dy = abs(y2-y1)**2
        distance=math.sqrt(dx+dy)
        return distance
    calcDistance = staticmethod(calcDistance) 
    
    @staticmethod
    def adapt_To_Max_Index(number, max):   # Empêche de dépasser l'index maximum d'un array
        if number >= max:
            number = max -1            
        return number

    @staticmethod
    def adapt_To_Min_Index(number):    # Empêche de dépasser un array vers les négatif
        if number < 0:
            number = 0            
        return number
    
    @staticmethod
    def adapt_To_Array_Size(number,max):    # Fais les deux méthodes plus haut d'un seul coup
        if number >= max:
            number = max -1
        else:
            if number < 0:
                number = 0
        return number
    
    @staticmethod
    def adapt_To_Max_Value(number, max):   # Empêche de dépasser le maximum
        if number > max:
            number = max            
        return number
    
    @staticmethod
    def adapt_To_Min_Value(number, min):   # Empêche de dépasser le minimum
        if number < min:
            number = min            
        return number
    
    @staticmethod
    def adapt_To_Interval(number, min):   # Fais les deux méthodes plus haut d'un seul coup
        if number > max:
            number = max    
        elif number < min:            
            number = min            
        return number
    
    @staticmethod
    def Roll_50():
        return random.choice([0, 1]) == 0   # 50% de chance
    
    @staticmethod
    def Roll_50(choice1,choice2):
        return random.choice([choice1, choice2])   # 50
    
    @staticmethod
    def Rand_Sign():
        return random.choice([1,-1])        #50% de chance d'être positif ou négatif

if __name__ == '__main__':
    nb = 3
    nb = Helper.adapt_To_Max_Index(nb, 3)
    print(nb)
    nb = Helper.adapt_To_Min_Index(-2)
    print(nb)