# TP2 - Equipe 6
# Alex Bouchard
# Anthony Sanou
# Gabriel Kirouac
# Hiba Ikbal
# Laurent Montreuil
# Ouail Slimani
# Wilfrido Quinteros Ramirez
# William Michaud

from tkinter import *



class Vue():
    def __init__(self,parent):
        self.parent=parent
        self.modele=None
        self.largeur=800#modele.largeurpx
        self.hauteur=600#modele.hauteurpx
        self.taillecase=25 #modele.taillecase
        self.zoom=1
        self.grilleon=0
        self.optionscourantes={}
        self.root=Tk()
        self.creercadrecarte()
        self.creercadreoutils()
        self.slider=None
       
       
       
    def creercadrecarte(self):
        self.carte=Frame(self.root)
        self.canevas=Canvas(self.carte,width=self.largeur,height=self.hauteur,bg="khaki1")
        self.canevas.pack(expand=1,fill=BOTH)
        self.carte.pack(side=LEFT,expand=1,fill=BOTH)
        
    def creeroptionscourantes(self,options):
        for i,j in enumerate(options):
            print(i,j)
            lab=Label(self.cadreoptions,text=options[j][0])
            ent=Entry(self.cadreoptions)
            ent.insert(0,options[j][1])
            lab.grid(column=0,row=i)
            ent.grid(column=1,row=i)
            self.optionscourantes[j]=ent
            
    def creercadreoutils(self):
        self.cadreoutils=Frame(self.root)
        self.cadrenouvellesimulation=Frame(self.cadreoutils)
        self.cadreoptions=Frame(self.cadrenouvellesimulation)
        options={"taillex":["Taille en X","1200"],
                 "tailley":["Taille en Y","1000"],
                 "taillecase":["Taille des cases","10"],
                 "initrnd":["Init pour aleatoire","2571"],
                 "nbforets":["Nb de forets","5"],
                 "nbplansdeau":["Nb de plans d'eau","3"],
                 "nbdeserts":["Nb deserts","3"],
                 "nbplaines":["Nb de plaines","10"],
                 "nbmontagnes":["Nb de montagnes","5"],
                 "nbarbres":["Nb d'arbres","200"],
                 "nbframboisiers":["Nb framboisiers", "30"],
                 "nbLegumes":["Nb legumes","20"],
                 "nblapins":["Nb de lapins","50"],
                 "nbloups":["Nb de loups","50"]}
        
        self.creeroptionscourantes(options)
        self.vitesseSim=IntVar()
        self.vitesseSim.set(1)
        self.slider= Scale(self.root, from_=1, to=10, variable=self.vitesseSim)
        self.slider.pack()
        btndemarre=Button(self.cadrenouvellesimulation,text="Demarrer",command=self.demarrersimulation)
        btndemarre.pack()
        
        
        self.cadrevisiongrille=Frame(self.cadreoutils)
        self.btnloupe=Button(self.cadrevisiongrille,text="Vision ciblee",command=self.ciblevision)
        self.btnloupe.pack()
        self.btnloupe=Button(self.cadrevisiongrille,text="Grille",command=self.dessinegrille)
        self.btnloupe.pack()
        
        self.cadreoptions.pack()
        self.cadrenouvellesimulation.pack()
        self.cadrevisiongrille.pack()
        self.cadreoutils.pack(side=LEFT)
       
    def demarrersimulation(self):
        taillex=self.optionscourantes["taillex"].get()
        self.slider=self.parent.TimeRatioControlleur
        tailley=self.optionscourantes["tailley"].get()
        taillecase=self.optionscourantes["taillecase"].get()
        initrnd=self.optionscourantes["initrnd"].get()
        nbforets=self.optionscourantes["nbforets"].get()
        nbplansdeau=self.optionscourantes["nbplansdeau"].get()
        nbframboisiers=self.optionscourantes["nbframboisiers"].get()
        nbLegumes=self.optionscourantes["nbLegumes"].get()
        nbdeserts=self.optionscourantes["nbdeserts"].get()
        nbplaines=self.optionscourantes["nbplaines"].get()
        nbmontagnes=self.optionscourantes["nbmontagnes"].get()
        nbarbres=self.optionscourantes["nbarbres"].get()
        nblapins=self.optionscourantes["nblapins"].get()
        nbloups=self.optionscourantes["nbloups"].get()
        self.parent.demarrersimulation(taillex,tailley,taillecase,initrnd,
                                       nbforets,nbplansdeau,nbdeserts,nbplaines,
                                       nbmontagnes,nbarbres,nblapins,nbloups,
                                       nbframboisiers,nbLegumes)
        
    def ciblevision(self):
        if self.parent.ciblevision!=1:
            self.canevas.bind("<Motion>",self.bougeloupe)
        else:
            self.canevas.unbind("<Motion>")
        self.parent.ciblevision=self.parent.ciblevision*-1
        print(self.parent.ciblevision)  
       
    def bougeloupe(self,evt):
        self.parent.setsubcourant(evt.x,evt.y)
       
    def dessinegrille(self):
        if self.grilleon==0:
            for i in range(int(self.hauteur/self.modele.taillecase)):
                self.canevas.create_line(0,i*self.modele.taillecase,self.largeur,i*self.modele.taillecase,tags=("grille",))
            for i in range(int(self.largeur/self.modele.taillecase)):
                self.canevas.create_line(i*self.modele.taillecase,0,i*self.modele.taillecase,self.hauteur,tags=("grille",))
            self.grilleon=1
        else:
            self.grilleon=0
            self.canevas.delete("grille")
           
    def dessineterrain(self,map):
        self.canevas.delete(ALL)
        for y,j, in enumerate(map):
            for x,i in enumerate(j):
                if i["terrain"]=="forets":
                    self.canevas.create_rectangle(x*self.modele.taillecase,y*self.modele.taillecase,
                                                  (x*self.modele.taillecase)+self.modele.taillecase,
                                                  (y*self.modele.taillecase)+self.modele.taillecase,fill="lightgreen",
                                                  outline="lightgreen",tags=("terrain","forets"))
                elif i["terrain"]=="plansdeau":
                    self.canevas.create_rectangle(x*self.modele.taillecase,y*self.modele.taillecase,
                                                  (x*self.modele.taillecase)+self.modele.taillecase,
                                                  (y*self.modele.taillecase)+self.modele.taillecase,fill="lightblue",
                                                  outline="lightblue",tags=("terrain","plansdeau"))
                elif i["terrain"]=="deserts":
                    self.canevas.create_rectangle(x*self.modele.taillecase,y*self.modele.taillecase,
                                                  (x*self.modele.taillecase)+self.modele.taillecase,
                                                  (y*self.modele.taillecase)+self.modele.taillecase,fill="darkgrey",
                                                  outline="darkgrey",tags=("terrain","deserts"))
                elif i["terrain"]=="plaines":
                    self.canevas.create_rectangle(x*self.modele.taillecase,y*self.modele.taillecase,
                                                  (x*self.modele.taillecase)+self.modele.taillecase,
                                                  (y*self.modele.taillecase)+self.modele.taillecase,fill="lightgreen",
                                                  outline="lightgreen",tags=("terrain","plaines"))
                elif i["terrain"]=="montagnes":
                    self.canevas.create_rectangle(x*self.modele.taillecase,y*self.modele.taillecase,
                                                  (x*self.modele.taillecase)+self.modele.taillecase,
                                                  (y*self.modele.taillecase)+self.modele.taillecase,fill="grey",
                                                  outline="grey",tags=("terrain","montagnes"))



   
    def dessinecarte(self,map):
        self.canevas.delete("dynamique")
        if self.parent.nbBoucle%3==0:
            self.canevas.delete("semidynamique")
            vegeOn=1
        else:
            vegeOn=0
        for i in map:
            for j in i:
                if vegeOn:
                    for k in  j["arbres"]:
                        self.canevas.create_oval(k.x-k.size,k.y-k.size,k.x+k.size,k.y+k.size,fill="green",tags=("semidynamique","arbres"))
                    for k in j["framboisiers"]:
                        self.canevas.create_oval(k.x-k.size,k.y-k.size,k.x+k.size,k.y+k.size,fill="pink",tags=("semidynamique","framboisiers"))
                    for k in j["legumes"]:
                        self.canevas.create_oval(k.x-k.size,k.y-k.size,k.x+k.size,k.y+k.size,fill="orange",tags=("semidynamique","legumes"))
                for k in j["framboisiers"]:
                    self.canevas.create_oval(k.x-k.size,k.y-k.size,k.x+k.size,k.y+k.size,fill="pink",tags=("semidynamique","framboisiers"))
                for k in  j["lapins"]:
                    self.canevas.create_oval(k.x,k.y,k.x+6,k.y+6,fill="red",tags=("dynamique","lapins"))
                for k in  j["loups"]:
                    self.canevas.create_oval(k.x,k.y,k.x+12,k.y+12,fill="lightblue",tags=("dynamique","loups"))